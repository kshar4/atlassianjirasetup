# Jira Installation

This template can be used for Installing Jira server using CloudFormation template. The app is deployed to a Amazon EC2 server and will expose the endpoint `yourip:8080`. The database connected to Jira server is Aurora PostgresSql

## VPC Setup

VPC with private and public subnets in two Availability Zones
- 2 Public Subnets
- 2 Private Subnets
- Private and Public Route Tables for traffic in Subnets
- NAT Gateway - Private route table can access web via NAT Gateway
- Internet Gateway 
- EC2 instance for hosting Jira web server
- Clusters for Aurora PostgresSql 

### Assumptions ###
- EC2 key file is already created on the AWS EC2 space

### Execution ###
```
This template is successfully executed using aws cli
`aws cloudformation create-stack --stack-name AtlassianJiraServer --template-body file://jira_cloud_setup.yml --region us-east-1`
```
### Missing Outputs ###
```
1. Jira server installation works fine when ssh to public instance (app server) and executing the shell script code mentioned from Line 178-185.
2. During template execution, we are referring to the file {response.varfile} for unattended Jira server install, the script runs fine without any error (No errors in `/var/log/cloud-init-output.log`.), but server setup fails to generate Jira Home directory files. 
   Assuming, due to permissions issue for the root user
```
### Missing Use Case ###
* Little bit aware of ansible, tried hands on it but still not comformatable to fulfil the condition of using a playbook inside the cloudformation template
* Custom hostname was not set through Route53 due to time limitation